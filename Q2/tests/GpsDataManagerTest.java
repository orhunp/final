import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class GpsDataManagerTest {
    private static final String USER_DIR = String.format("%s%s", System.getProperty("user.dir"), File.separator);

    @Test
    void getGpsList() {
        ArrayList<GpsData> gpsList = new GpsDataManager().getGpsList(USER_DIR + "gps_data.csv");
        assertEquals(25186, gpsList.size());
    }
}