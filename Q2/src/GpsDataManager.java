import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

class GpsDataManager {
    ArrayList<GpsData> getGpsList(String file) {
        ArrayList<GpsData> arrayList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                GpsData gpsData = new GpsData();
                String[] data = line.split(";");
                gpsData.setJdate(data[0]);
                gpsData.setTimeStamp(data[1]);
                gpsData.setSatellite(data[2]);
                gpsData.setLat(data[3]);
                gpsData.setLon(data[4]);
                gpsData.setStec(Integer.parseInt(data[5]));
                gpsData.setVtec(Double.parseDouble(data[6]));
                arrayList.add(gpsData);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }
}
