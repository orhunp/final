import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

class GpsDataManager {

    private ArrayList<GpsData> gpsDataArrayList;

    GpsDataManager(String file) {
        this.gpsDataArrayList = getGpsList(file);
    }

    private ArrayList<GpsData> getGpsList(String file) {
        ArrayList<GpsData> arrayList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                GpsData gpsData = new GpsData();
                String[] data = line.split(";");
                gpsData.setJdate(data[0]);
                gpsData.setTimeStamp(data[1]);
                gpsData.setSatellite(data[2]);
                gpsData.setLat(data[3]);
                gpsData.setLon(data[4]);
                gpsData.setStec(Integer.parseInt(data[5]));
                gpsData.setVtec(Double.parseDouble(data[6]));
                arrayList.add(gpsData);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    ArrayList<GpsData> getGpsDataArrayList() {
        return gpsDataArrayList;
    }

    private Set<String> getTimestampSet() {
        Set<String> timestampSet = new HashSet<>();
        for (GpsData gps: this.gpsDataArrayList) {
            timestampSet.add(gps.getTimeStamp());
        }
        return timestampSet;
    }

    Map<String, List<GpsData>> getGpsDataMap() {
        Map<String, List<GpsData>> gpsDataMap = new HashMap<>();
        for (String timestamp: getTimestampSet()) {
            List <GpsData> gpsDataList = new ArrayList<>();
            for (GpsData gps: this.gpsDataArrayList) {
                if (timestamp.equals(gps.getTimeStamp())) {
                    gpsDataList.add(gps);
                }
            }
            gpsDataMap.put(timestamp, gpsDataList);
        }
        return gpsDataMap;
    }

    @SuppressWarnings("unused")
    void printAvgStec() {
        MathManager<Integer> mathManager = new MathManager<>();
        for(Map.Entry<String, List<GpsData>> entry : getGpsDataMap().entrySet()) {
            ArrayList<Integer> stecList = new ArrayList<>();
            for (GpsData gpsData: entry.getValue()) {
                stecList.add(gpsData.getStec());
            }
            System.out.println(String.format("Avg of %s is %.2f", entry.getKey(), mathManager.avg(stecList)));
        }
    }

    void printMinVtec() {
        MathManager<Double> mathManager = new MathManager<>();
        for(Map.Entry<String, List<GpsData>> entry : getGpsDataMap().entrySet()) {
            ArrayList<Double> vtecList = new ArrayList<>();
            for (GpsData gpsData: entry.getValue()) {
                vtecList.add(gpsData.getVtec());
            }
            System.out.println(String.format("Min of %s is %.2f", entry.getKey(), mathManager.min(vtecList)));
        }
    }
}
