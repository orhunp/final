
public class GpsData {

    private String jdate;

    private String timeStamp;

    private String satellite;

    private String lon;

    private String lat;

    private Integer stec;

    private Double vtec;

    public String getJdate() {
        return jdate;
    }

    public void setJdate(String jdate) {
        this.jdate = jdate;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getSatellite() {
        return satellite;
    }

    public void setSatellite(String satellite) {
        this.satellite = satellite;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public Integer getStec() {
        return stec;
    }

    public void setStec(Integer stec) {
        this.stec = stec;
    }

    public Double getVtec() {
        return vtec;
    }

    public void setVtec(Double vtec) {
        this.vtec = vtec;
    }
}