import java.io.File;

public class Main {
    private static final String USER_DIR = String.format("%s%s", System.getProperty("user.dir"), File.separator);

    public static void main(String[] args) {
        GpsDataManager gpsList = new GpsDataManager(USER_DIR + "gps_data.csv");
        gpsList.printMinVtec();
    }
}
