import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class GpsDataManagerTest {
    private static final String USER_DIR = String.format("%s%s", System.getProperty("user.dir"), File.separator);
    private static final int FILE_SIZE = 25186;

    @Test
    void getGpsList() {
        GpsDataManager gpsList = new GpsDataManager(USER_DIR + "gps_data.csv");
        int fileSize = 0;
        for(Map.Entry<String, List<GpsData>> entry : gpsList.getGpsDataMap().entrySet()) {
            fileSize += entry.getValue().size();
        }
        assertEquals(FILE_SIZE, fileSize);
        assertEquals(FILE_SIZE, gpsList.getGpsDataArrayList().size());
    }
}