import java.util.Collection;

interface MathOperations<N> {
    N min(Collection<N> numberList);
    N max(Collection<N> numberList);
    N sum(Collection<N> numberList);
    N avg(Collection<N> numberList);
}
