import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MathManagerTest {
    @Test
    void min() {
        MathManager<Integer> mathManager = new MathManager<>();
        List<Integer> list = Arrays.asList(16, 23, 54, 1);
        assertEquals(1, mathManager.min(list).intValue());
    }

    @Test
    void max() {
        MathManager<Double> mathManager = new MathManager<>();
        List<Double> list = Arrays.asList(16.2, 23.4, 54.12, 134.5);
        assertEquals(134.5, mathManager.max(list).doubleValue());
    }

    @Test
    void sum() {
        MathManager<Float> mathManager = new MathManager<>();
        List<Float> list = Arrays.asList(16.2f, 23.2f);
        assertEquals(39, mathManager.sum(list).intValue());
    }

    @Test
    void avg() {
        MathManager<Float> mathManager = new MathManager<>();
        List<Float> list = Arrays.asList(15.0f, 25.0f, 10.0f, 30.0f);
        assertEquals(20, mathManager.avg(list).intValue());
    }
}