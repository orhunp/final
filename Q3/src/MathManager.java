import java.util.*;

@SuppressWarnings("unchecked")
public class MathManager<N extends Number & Comparable<N>> implements MathOperations<N> {
    @Override
    public N min(Collection<N> numberList) {
        return numberList.stream().min(Comparator.naturalOrder()).orElseThrow(NoSuchElementException::new);
    }

    @Override
    public N max(Collection<N> numberList) {
        return numberList.stream().max(Comparator.naturalOrder()).orElseThrow(NoSuchElementException::new);
    }

    @Override
    public N sum(Collection<N> numberList) {
        return (N) new Float(numberList.stream().mapToDouble(Number::doubleValue).sum());
    }

    @Override
    public N avg(Collection<N> numberList) {
        return (N) new Float(numberList.stream().mapToDouble(Number::doubleValue).sum() / numberList.size());
    }
}
